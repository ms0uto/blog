from flask import Blueprint
from flask_restx import Resource, Api


index_blueprint = Blueprint('index', __name__)
api = Api(index_blueprint)


class Index(Resource):
    def get(self):
        return {
            'status': 'success',
            'message': 'Pam!'
        }


api.add_resource(Index, '/index')
